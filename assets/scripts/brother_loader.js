// Function which loads in all the brothers in their year based on the brothers.json file
// Each brother has a name, year, and image field.
// name (str) -> Brother name
// year (str) -> Class year (senior, junior, etc.)
// image (bool) -> Whether or not we have a picture on file
function loadBrothers() {
    $.getJSON("/assets/json/brothers.json", function(data) {
		for (let i = 0; i < data.length; i++) {
			// Generates the initial figure object
			var appendStr = '<figure class="figure col profile-image"><img class="figure-img img-fluid" src="/assets/img/brothers/';

			// Check to see if we have image on file -- If so then display that -- If not then display placeholder
			if (data[i].image) appendStr += data[i].name.toUpperCase().replace(/ /g, '+') + '.jpg"'; 
			else appendStr += 'placeholder.jpg"';

			// Close off the figure object
			appendStr += 'alt="' + data[i].name + '"><figcaption class="figure-caption">' + data[i].name + '</figcaption></figure>';
			$('#' + data[i].year).append(appendStr);
		}
    });
}

function loadOfficers() {
	var officeTitles = {	
		"president": "President",
		"vice_president": "Vice President",
		"secretary": "Secretary",
		"treasurer": "Treasurer",
		"rush_chair": "Rush Chair",
		"social_chair": "Social Chair",
		"house_manager": "House Manager",
		"elmo_chair": "Elmo Chair",
		"alumni_chair": "Alumni Chair",
		"goat": "G.O.A.T.",
		"scholarship": "Scholarship",
		"ifc_representative": "IFC Representative",
		"philanthropy_chair": "Philanthropy Chair",
		"athletics": "Athletics",
		"pr_chair": "PR Chair"
	};

	$.getJSON("/assets/json/officers.json", function (data) {
		for (var key in data) {
			$('#officers > ul').append('<li id="' + key + '"><strong>' + officeTitles[key] + ': </strong>' + data[key] + '</li>')
		}
	});
}

function resizeBlurbMargin() {
	if (window.innerWidth > 1000) $("#blurb").addClass("blurb-shrink");
	$(window).resize(function() {
		if (window.innerWidth > 1000) $("#blurb").addClass("blurb-shrink");
		else $("#blurb").removeClass("blurb-shrink");
	});

}

$(document).ready(function() {
	resizeBlurbMargin();
	loadOfficers();
    loadBrothers();
})
